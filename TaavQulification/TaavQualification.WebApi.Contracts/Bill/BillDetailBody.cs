﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TaavQualification.WebApi.Contracts.Bill
{
    public class BillDetailBody
    {
        public Guid ProductId { get; set; }
        public int? Quantity { get; set; }
        public int? Price { get; set; }
        public bool QuantityValidation { get; set; }
        public bool StockValidation { get; set; }
    }
}
