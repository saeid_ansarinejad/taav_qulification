﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TaavQualification.WebApi.Contracts.Bill
{
    public class SalesBillResponse
    {
        public Guid ProductId { get; set; }
        public string Message { get; set; }
    }
}
