﻿using System;
using System.Collections.Generic;

namespace TaavQualification.WebApi.Contracts.Bill
{
    public class BillBody
    {
        public int? Number { get; set; }
        public DateTime? Date { get; set; } = DateTime.Now;
        public string DisplayDate { get; set; }
        public string Name { get; set; }
        public bool SalesBill { get; set; }
        public int? DocNumber { get; set; }
        public DateTime? DocDate { get; set; } = DateTime.Now;
        public string DocDisplayDate { get; set; }
        public List<BillDetailBody> BillDetails { get; set; }
    }
}
