﻿
namespace TaavQualification.WebApi.Contracts.ProductCategory
{
    public class ProductCategoryBody
    {
        public string Title { get; set; }
    }
}
