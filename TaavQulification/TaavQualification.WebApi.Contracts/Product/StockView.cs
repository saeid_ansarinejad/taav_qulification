﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TaavQualification.WebApi.Contracts.Product
{
    public class StockView
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public int? MinStock { get; set; }
        public string Category { get; set; }
        public int? StockQuantity { get; set; }
        public string Status { get; set; }
    }
}
