﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TaavQualification.WebApi.Contracts.Product
{
    public class ProductBody
    {
        public Guid ProductCategoryId { get; set; }
        public string Title { get; set; }
        public int? MinStock { get; set; }
        public int? Price { get; set; }
    }
}
