﻿using System;
using Microsoft.AspNetCore.Mvc;
using Entities = TaavQualification.WebApi.DataAccess.Entities;
using TaavQualification.WebApi.DataAccess.UnitOfWorks;
using System.Linq;
using TaavQualification.WebApi.Contracts.ProductCategory;

namespace TaavQualification.WebApi.Controllers
{
    [ApiVersion("1.0")]
    [ApiController]
    [Route("api/v{version:apiVersion}/taav/productcategory")]
    public class ProductCategoryController : ControllerBase
    {
        private readonly IUnitOfWork _unitofwork;
        public ProductCategoryController(IUnitOfWork unitOfWork)
        {
            _unitofwork = unitOfWork;
        }

        [MapToApiVersion("1.0")]
        [HttpPost]
        public IActionResult POST([FromBody]ProductCategoryBody productCategoryBody)
        {
            try
            {
                if (productCategoryBody == null)
                {
                    return BadRequest();
                }
                var productCat = _unitofwork.ProductCategoryRepository.All.FirstOrDefault(z => z.Title == productCategoryBody.Title);
                if (productCat != null)
                {
                    return Conflict();
                }

                _unitofwork.ProductCategoryRepository.AddAsync(new Entities.ProductCategory() { Title = productCategoryBody.Title });
                return Ok();
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        [MapToApiVersion("1.0")]
        [HttpPut]
        [Route("{id}")]

        public IActionResult PUT([FromBody]ProductCategoryBody productCategoryBody, Guid id)
        {
            try
            {
                if (productCategoryBody == null)
                {
                    return BadRequest();
                }


                var productCat = _unitofwork.ProductCategoryRepository.All.FirstOrDefault(z => z.Title == productCategoryBody.Title && z.Id != id);
                if (productCat != null)
                    return Conflict();

                productCat = _unitofwork.ProductCategoryRepository.Find(id);
                if (productCat == null)
                    return NotFound();

                productCat.Title = productCategoryBody.Title;
                _unitofwork.ProductCategoryRepository.Update(productCat);
                return Ok();
            }
            catch (Exception ex)
            {

                throw;
            }
        }
    }
}