﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using TaavQualification.WebApi.Contracts.Bill;
using TaavQualification.WebApi.DataAccess.UnitOfWorks;
using TaavQualification.WebApi.Extensions;
using Entities = TaavQualification.WebApi.DataAccess.Entities;

namespace TaavQualification.WebApi.Controllers
{
    [ApiVersion("1.0")]
    [ApiController]
    [Route("api/v{version:apiVersion}/taav/bill")]
    public class BillController : ControllerBase
    {
        private readonly IUnitOfWork _unitofwork;
        private bool _billHasValidItem;
        public BillController(IUnitOfWork unitOfWork)
        {
            _unitofwork = unitOfWork;
            _billHasValidItem = false;
        }

        [MapToApiVersion("1.0")]
        [HttpPost]
        public IActionResult POST([FromBody]BillBody billBody)
        {
            try
            {
                if (billBody == null)
                {
                    return BadRequest();
                }
                Entities.Bill bill;
                if (billBody.SalesBill)
                {
                    _billHasValidItem = Helper.ValidateBillDetails(_unitofwork.ProductRepository.All, _unitofwork.BillDetailRepository.All, ref billBody);
                    if (_billHasValidItem)
                    {
                        bill = new Entities.Bill()
                        {
                            Date = billBody.Date,
                            DisplayDate = billBody.DisplayDate,
                            DocDate = DateTime.Now,
                            DocDisplayDate = "1399/03/21",//must be callculated based on DocDate
                            DocNumber = Helper.GetNextDocNumber(_unitofwork.BillRepository.All),
                            Name = billBody.Name,
                            Number = Helper.GetBillNumber(_unitofwork.BillRepository.All),
                            SalesBill = true
                        };
                        _unitofwork.BillRepository.AddAsync(bill);
                        List<SalesBillResponse> responses = new List<SalesBillResponse>();
                        foreach (var item in billBody.BillDetails)
                        {
                            if (item.QuantityValidation && item.StockValidation)
                            {
                                var billDetail = new Entities.BillDetail()
                                {
                                    BillId = bill.Id,
                                    Price = item.Price,
                                    Quantity = item.Quantity,
                                    ProductId = item.ProductId
                                };
                                _unitofwork.BillDetailRepository.AddAsync(billDetail);
                            }
                            else
                            {
                                if (!item.StockValidation)
                                    responses.Add(new SalesBillResponse() { ProductId = item.ProductId, Message = "موجودی کافی نیست" });
                                if (!item.QuantityValidation)
                                    responses.Add(new SalesBillResponse() { ProductId = item.ProductId, Message = "حجم سفارش بیش از حد مجاز است" });

                            }
                        }
                        if (responses.Count > 0)
                            return Ok(responses);
                        return Ok();
                    }
                    return BadRequest("هیچ آیتم مجازی در فاکتور وجود نداشت");
                }
                bill = new Entities.Bill()
                {
                    Date = billBody.Date,
                    DisplayDate = billBody.DisplayDate,
                    Name = billBody.Name,
                    Number = billBody.Number,
                    SalesBill = false
                };
                _unitofwork.BillRepository.AddAsync(bill);

                foreach (var item in billBody.BillDetails)
                {
                    var billDetail = new Entities.BillDetail()
                    {
                        BillId = bill.Id,
                        Price = item.Price,
                        Quantity = item.Quantity,
                        ProductId = item.ProductId
                    };
                    _unitofwork.BillDetailRepository.AddAsync(billDetail);
                }
                return Ok();
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }


    }
}