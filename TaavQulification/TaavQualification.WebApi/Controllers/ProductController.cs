﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using TaavQualification.WebApi.Contracts.Product;
using TaavQualification.WebApi.DataAccess.UnitOfWorks;
using TaavQualification.WebApi.Extensions;
using Entities = TaavQualification.WebApi.DataAccess.Entities;

namespace TaavQualification.WebApi.Controllers
{
    [ApiVersion("1.0")]
    [ApiController]
    [Route("api/v{version:apiVersion}/taav/product")]
    public class ProductController : ControllerBase
    {
        private readonly IUnitOfWork _unitofwork;
        private int _pageSize = Startup.PageSize;

        public ProductController(IUnitOfWork unitOfWork)
        {
            _unitofwork = unitOfWork;
        }

        [MapToApiVersion("1.0")]
        [HttpPost]
        public IActionResult POST([FromBody]ProductBody productBody)
        {
            try
            {
                if (productBody == null)
                {
                    return BadRequest();
                }
                var product = _unitofwork.ProductRepository.All.FirstOrDefault(z => z.Title == productBody.Title &&
                        z.ProductCategoryId == productBody.ProductCategoryId);
                if (product != null)
                {
                    return Conflict();
                }

                _unitofwork.ProductRepository.AddAsync(new Entities.Product()
                {
                    Title = productBody.Title,
                    ProductCategoryId = productBody.ProductCategoryId,
                    MinStock = productBody.MinStock,
                    Price = productBody.Price
                });
                return Ok();
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        [MapToApiVersion("1.0")]
        [HttpPut]
        [Route("{id}")]

        public IActionResult PUT([FromBody]ProductBody productBody, Guid id)
        {
            try
            {
                if (productBody == null)
                {
                    return BadRequest();
                }


                var product = _unitofwork.ProductRepository.All.FirstOrDefault(z => z.Title == productBody.Title
                && z.ProductCategoryId == productBody.ProductCategoryId && z.Id != id);
                if (product != null)
                    return Conflict();

                product = _unitofwork.ProductRepository.Find(id);
                if (product == null)
                    return NotFound();

                product.Title = productBody.Title;
                product.ProductCategoryId = productBody.ProductCategoryId;
                product.MinStock = productBody.MinStock;
                product.Price = productBody.Price;

                _unitofwork.ProductRepository.Update(product);
                return Ok();
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        [MapToApiVersion("1.0")]
        [HttpGet]
        [PageFilter]
        public IActionResult GET()
        {
            try
            {
                Regex regex = new Regex(@"^[0-9]+$");
                var page_size = HttpContext.Request.Headers["page-size"].ToString();
                if (page_size != "null" && !regex.IsMatch(page_size))
                {
                    return BadRequest("شماره صفحه نامعتبر است");
                }
                if (page_size != "null")
                {
                    _pageSize = Convert.ToInt32(page_size);
                }
                if (_pageSize <= 0)
                {
                    return BadRequest("شماره صفحه نامعتبر است");
                }
                var query = _unitofwork.ProductRepository.AllByInclude("ProductCategory,BillDetails");
                var billDet = _unitofwork.BillDetailRepository.All.AsQueryable();
                List<StockView> stockViews = new List<StockView>();
                foreach (var item in query)
                {
                    var stock = Helper.GetStockQuantity(billDet, item.Id);
                    stockViews.Add(new StockView()
                    {
                        Id = item.Id,
                        Category = item.ProductCategory.Title,
                        MinStock = item.MinStock,
                        StockQuantity = stock,
                        Title = item.Title,
                        Status = (stock == 0 ? "ناموجود" : (stock <= item.MinStock ? "آماده سفارش" : "موجود"))
                    });
                }

                var filterCol = HttpContext.Request.Headers["filter-column"].ToString();
                var filterVal = HttpContext.Request.Headers["filter-value"].ToString();
                if (filterCol != "null" && filterVal != "null")
                {
                    switch (filterCol)
                    {
                        case "id":
                            stockViews = stockViews.Where(z => z.Id.ToString().Contains(filterVal)).ToList();
                            break;

                        case "title":
                            stockViews = stockViews.Where(z => z.Title.Contains(filterVal)).ToList();
                            break;

                        case "minstock":
                            stockViews = stockViews.Where(z => z.MinStock.ToString().Contains(filterVal)).ToList();
                            break;

                        case "category":
                            stockViews = stockViews.Where(z => z.Category.Contains(filterVal)).ToList();
                            break;

                        case "stockquantity":
                            stockViews = stockViews.Where(z => z.StockQuantity.ToString().Contains(filterVal)).ToList();
                            break;

                        case "status":
                            stockViews = stockViews.Where(z => z.Status.Contains(filterVal)).ToList();
                            break;
                    }
                }

                var sort = HttpContext.Request.Headers["sort"].ToString();
                switch (sort)
                {
                    case "id":
                        stockViews = stockViews.OrderBy(z => z.Id).ToList();
                        break;

                    case "title":
                        stockViews = stockViews.OrderBy(z => z.Title).ToList();
                        break;

                    case "minstock":
                        stockViews = stockViews.OrderBy(z => z.MinStock).ToList();
                        break;

                    case "category":
                        stockViews = stockViews.OrderBy(z => z.Category).ToList();
                        break;

                    case "stockquantity":
                        stockViews = stockViews.OrderBy(z => z.StockQuantity).ToList();
                        break;

                    case "status":
                        stockViews = stockViews.OrderBy(z => z.Status).ToList();
                        break;
                }


                var queryCount = stockViews.Count();
                var page = 0;
                if (int.TryParse(HttpContext.Request.Headers["page"], out page))
                {
                    if (queryCount == 0 && page != 1)
                    {
                        return NotFound("صفحه پیدا نشد");
                    }
                    if (queryCount == 0)
                    {
                        Response.ResponseSetPage(queryCount, page);

                        return Ok(query);
                    }
                    var maxPages = queryCount / _pageSize;
                    var mode = queryCount % _pageSize;
                    if (mode != 0)
                    {
                        maxPages++;
                    }
                    if (page <= 0 || page > maxPages)
                    {
                        return NotFound();
                    }
                    stockViews = stockViews.Skip((page - 1) * _pageSize)
                       .Take(_pageSize).ToList();


                    Response.ResponseSetPage(queryCount, page);
                    return Ok(stockViews);
                }
                else
                {
                    var x_page = HttpContext.Request.Headers["page"].ToString();
                    if (!regex.IsMatch(x_page) && x_page != "null")
                    {
                        return NotFound("صفحه پیدا نشد");
                    }

                    Response.ResponseSetPage(queryCount, page);
                    return Ok(stockViews);
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }
    }
}