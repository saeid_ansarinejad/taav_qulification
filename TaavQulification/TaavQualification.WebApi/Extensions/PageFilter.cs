﻿using Microsoft.AspNetCore.Mvc.Filters;
using System;

namespace TaavQualification.WebApi.Extensions
{
    public class PageFilter : Attribute, IActionFilter
    {
        public void OnActionExecuted(ActionExecutedContext context)
        {

        }

        public void OnActionExecuting(ActionExecutingContext context)
        {
            var headers = context.HttpContext.Request.Headers;
            if (!headers.ContainsKey("page"))
            {
                context.HttpContext.Request.Headers.Add("page", "null");
            }
            if (!headers.ContainsKey("page-size"))
            {
                context.HttpContext.Request.Headers.Add("page-size", "null");
            }
        }
    }
}
