﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TaavQualification.WebApi.Contracts.Bill;
using TaavQualification.WebApi.DataAccess.Entities;

namespace TaavQualification.WebApi.Extensions
{
    public static class Helper
    {
        private static bool _billHasValidItem = false;
        public static bool ValidateBillDetails(IQueryable<Product> products, IQueryable<BillDetail> billDetails, ref BillBody billBody)
        {
            foreach (var item in billBody.BillDetails)
            {
                item.StockValidation = item.Quantity <= GetStockQuantity(billDetails, item.ProductId);
                var prod = products.FirstOrDefault(z=>z.Id == item.ProductId);
                item.QuantityValidation = item.Quantity <= prod.MinStock;
                _billHasValidItem |= item.QuantityValidation && item.StockValidation;
            }
            return _billHasValidItem;
        }
        public static int GetStockQuantity(IQueryable<BillDetail> billDetails, Guid productId)
        {
            var prods = billDetails.Where(z => z.ProductId == productId);
            var sumSales = prods.Where(z => z.Bill.SalesBill)
                 .Select(z => z.Quantity)
                 .Sum();

            var sumEntries = prods.Where(z => !z.Bill.SalesBill)
                 .Select(z => z.Quantity)
                 .Sum();

            return sumEntries.Value - sumSales.Value;
        }

        public static int GetNextDocNumber(IQueryable<Bill> bills)
        {
            var max = bills.Where(z => z.SalesBill).Max(z => z.DocNumber);
            if (max.HasValue)
                return max.Value + 1;
            else
                return 1001;
        }

        public static int GetBillNumber(IQueryable<Bill> bills)
        {
            var max = bills.Where(z => z.SalesBill).Max(z => z.Number);
            if (max.HasValue)
                return max.Value + 1;
            else
                return 2001;
        }

        public static void ResponseSetPage(this HttpResponse response, int count, int page)
        {
            response.Headers.Add("x-count", count + "");
            response.Headers.Add("x-page", page + "");
        }
    }
}
