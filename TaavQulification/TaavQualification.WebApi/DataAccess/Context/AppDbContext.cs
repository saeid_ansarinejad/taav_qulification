﻿using Microsoft.EntityFrameworkCore;
using TaavQualification.WebApi.DataAccess.Entities;

namespace TaavQualification.WebApi.DataAccess.Context
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        {
        }

        #region DbSet
        public DbSet<ProductCategory> ProductCategories { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<Bill> Bills { get; set; }
        public DbSet<BillDetail> BillDetails { get; set; }

        #endregion

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            #region ProductCategory
            modelBuilder.Entity<ProductCategory>(builder =>
            {
                builder.ToTable("ProductCategories");
                builder.HasKey(x => x.Id);
                builder.Property(x => x.Title).IsRequired();
            });
            #endregion

            #region Product
            modelBuilder.Entity<Product>(builder =>
            {
                builder.ToTable("Products");
                builder.HasKey(x => x.Id);
                builder.Property(x => x.Title).IsRequired();
                builder.Property(x => x.MinStock).IsRequired();
                builder.Property(x => x.ProductCategoryId).IsRequired();
                builder.Property(x => x.Price).IsRequired();
            });
            #endregion

            #region Bill
            modelBuilder.Entity<Bill>(builder =>
            {
                builder.ToTable("Bills");
                builder.HasKey(x => x.Id);
                builder.Property(x => x.Name).IsRequired();
                builder.Property(x => x.Number).IsRequired();
                builder.Property(x => x.Date).IsRequired();
                builder.Property(x => x.DisplayDate).IsRequired();
                builder.Property(x => x.DocDate).IsRequired(false);
                builder.Property(x => x.DocDisplayDate).IsRequired(false);
                builder.Property(x => x.DocNumber).IsRequired(false);
                builder.Property(x => x.SalesBill).IsRequired();
            });
            #endregion
            
            #region BillDetail
            modelBuilder.Entity<BillDetail>(builder =>
            {
                builder.ToTable("BillDetails");
                builder.HasKey(x => x.Id);
                builder.Property(x => x.Price).IsRequired();
                builder.Property(x => x.Quantity).IsRequired();
                builder.Property(x => x.ProductId).IsRequired();
            });
            #endregion
        }
    }
}

