﻿using System;
using TaavQualification.WebApi.DataAccess.Repositories;

namespace TaavQualification.WebApi.DataAccess.UnitOfWorks
{
    public interface IUnitOfWork : IDisposable
    {
        ProductCategoryRepository ProductCategoryRepository { get; }
        ProductRepository ProductRepository { get; }
        BillRepository BillRepository { get; }

        BillDetailRepository BillDetailRepository { get; }
    }
}
