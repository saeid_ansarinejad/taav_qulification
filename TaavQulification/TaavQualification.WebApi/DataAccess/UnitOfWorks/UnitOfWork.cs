﻿using System;
using TaavQualification.WebApi.DataAccess.Context;
using TaavQualification.WebApi.DataAccess.Repositories;

namespace TaavQualification.WebApi.DataAccess.UnitOfWorks
{
    public class UnitOfWork : IUnitOfWork
    {

        private readonly AppDbContext _dbContext;
        private bool _disposed = false;
        public UnitOfWork(AppDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        #region repos

        private ProductCategoryRepository _productCategoryRepository;
        public ProductCategoryRepository ProductCategoryRepository => _productCategoryRepository = _productCategoryRepository ?? new ProductCategoryRepository(_dbContext);

        private ProductRepository _productRepository;
        public ProductRepository ProductRepository => _productRepository = _productRepository ?? new ProductRepository(_dbContext);
        
        private BillRepository _billRepository;
        public BillRepository BillRepository => _billRepository = _billRepository ?? new BillRepository(_dbContext);

        private BillDetailRepository _billDetailRepository;
        public BillDetailRepository BillDetailRepository => _billDetailRepository = _billDetailRepository ?? new BillDetailRepository(_dbContext);
        #endregion

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed && disposing)
            {
                _dbContext.Dispose();
            }
            _disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
