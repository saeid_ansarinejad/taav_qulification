﻿using System;
using System.Collections.Generic;

namespace TaavQualification.WebApi.DataAccess.Entities
{
    public class ProductCategory
    {
        public Guid Id { get; set; }
        public string Title { get; set; }

        public virtual List<Product> Products { get; set; }
    }
}
