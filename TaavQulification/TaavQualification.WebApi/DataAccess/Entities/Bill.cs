﻿using System;
using System.Collections.Generic;

namespace TaavQualification.WebApi.DataAccess.Entities
{
    public class Bill
    {
        public Guid Id { get; set; }
        public int? Number { get; set; }
        public DateTime? Date { get; set; } = DateTime.Now;
        public string DisplayDate { get; set; }
        public string Name { get; set; }
        public bool SalesBill { get; set; }
        public int? DocNumber { get; set; }
        public DateTime? DocDate { get; set; }
        public string DocDisplayDate { get; set; }

        public virtual List<BillDetail> BillDetails { get; set; }
    }
}
