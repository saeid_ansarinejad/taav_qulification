﻿using System;
using System.Collections.Generic;

namespace TaavQualification.WebApi.DataAccess.Entities
{
    public class Product
    {
        public Guid Id { get; set; }
        public Guid ProductCategoryId { get; set; }
        public string Title { get; set; }
        public int? MinStock { get; set; }
        public int? Price { get; set; }
        public virtual ProductCategory ProductCategory { get; set; }
        public virtual List<BillDetail> BillDetails { get; set; }
    }
}
