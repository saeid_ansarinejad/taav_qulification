﻿using TaavQualification.WebApi.DataAccess.Context;
using TaavQualification.WebApi.DataAccess.Entities;

namespace TaavQualification.WebApi.DataAccess.Repositories
{
    public class ProductCategoryRepository : Repository<ProductCategory>, IProductCategoryRepository
    {
        public ProductCategoryRepository(AppDbContext dbContext) : base(dbContext)
        {
        }
    }
}
