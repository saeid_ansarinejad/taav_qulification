﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;

namespace TaavQualification.WebApi.DataAccess.Repositories
{
    public interface IRepository<TEntity> where TEntity : class
    {
        IQueryable<TEntity> All { get; }
        IQueryable<TEntity> AllByInclude(string navigationPropertyPath);
        TEntity AddAsync(TEntity entity, CancellationToken cancellationToken = default);
        void Remove(TEntity entity);
        bool Remove(Expression<Func<TEntity, bool>> predicate);
        void Update(TEntity entity);
        void AddRangeAsync(IEnumerable<TEntity> entities, CancellationToken cancellationToken = default);
        TEntity Find(params object[] keyValues);
        Task<bool> AnyAsync(Expression<Func<TEntity, bool>> predicate);
        Task<bool> AnyAsync();

        void SaveChange();

    }
}
