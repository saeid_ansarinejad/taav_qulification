﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using TaavQualification.WebApi.DataAccess.Context;

namespace TaavQualification.WebApi.DataAccess.Repositories
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        private readonly AppDbContext _dbContext;
        private readonly DbSet<TEntity> _context;
        public Repository(AppDbContext dbContext)
        {
            _dbContext = dbContext;
            _context = _dbContext?.Set<TEntity>();

        }

        public IQueryable<TEntity> All => _context.AsQueryable();


        public IQueryable<TEntity> AllByInclude(string navigationPropertyPath)
        {
            var query = _context.AsQueryable();
            if (!string.IsNullOrWhiteSpace(navigationPropertyPath))
            {
                foreach (var item in navigationPropertyPath.Split(','))
                {
                    query = query.Include(item.Trim());
                }
            }
            return query;
        }


        public void AddRangeAsync(IEnumerable<TEntity> entities, CancellationToken cancellationToken = default)
        {
            ThrowIfDisposed();
            using (var dbContextTransaction = _dbContext.Database.BeginTransaction())
            {
                _context.AddRangeAsync(entities, cancellationToken);
                SaveChange();
                dbContextTransaction.Commit();
            }

        }

        public Task<bool> AnyAsync(Expression<Func<TEntity, bool>> predicate)
        {
            ThrowIfDisposed();
            return _context.AnyAsync(predicate);
        }

        public Task<bool> AnyAsync()
        {
            ThrowIfDisposed();
            return _context.AnyAsync();
        }
        
        public TEntity AddAsync(TEntity entity, CancellationToken cancellationToken = default)
        {
            ThrowIfDisposed();
            _context.AddAsync(entity, cancellationToken);
            SaveChange();
            return entity;
        }

        public void Remove(TEntity entity)
        {
            ThrowIfDisposed();
            _context.Remove(entity);
            SaveChange();
        }

        public bool Remove(Expression<Func<TEntity, bool>> predicate)
        {
            var entity = _context.FirstOrDefault(predicate);
            if (entity != null)
            {
                Remove(entity);
                return true;
            }
            return false;
        }

        public TEntity Find(params object[] keyValues)
        {
            return _context.Find(keyValues);
        }

        public void Update(TEntity entity)
        {
            ThrowIfDisposed();
            using (var dbContextTransaction = _dbContext.Database.BeginTransaction())
            {
                _dbContext.Update(entity);
                SaveChange();
                dbContextTransaction.Commit();
            }

        }

        public void SaveChange()
        {
            ThrowIfDisposed();
            _dbContext.SaveChanges();

        }

        private void ThrowIfDisposed()
        {
            if (_dbContext == null)
            {
                throw new ObjectDisposedException(GetType().Name);
            }
        }


    }
}
