﻿using TaavQualification.WebApi.DataAccess.Context;
using TaavQualification.WebApi.DataAccess.Entities;

namespace TaavQualification.WebApi.DataAccess.Repositories
{
    public class BillRepository : Repository<Bill>, IBillRepository
    {
        public BillRepository(AppDbContext dbContext) : base(dbContext)
        {
        }
    }
}
