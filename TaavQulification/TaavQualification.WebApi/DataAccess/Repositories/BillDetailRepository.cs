﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TaavQualification.WebApi.DataAccess.Context;
using TaavQualification.WebApi.DataAccess.Entities;

namespace TaavQualification.WebApi.DataAccess.Repositories
{
    public class BillDetailRepository : Repository<BillDetail>, IBillDetailRepository
    {
        public BillDetailRepository(AppDbContext dbContext) : base(dbContext)
        {
        }
    }
}
