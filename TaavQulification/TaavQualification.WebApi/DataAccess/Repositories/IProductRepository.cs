﻿using TaavQualification.WebApi.DataAccess.Entities;

namespace TaavQualification.WebApi.DataAccess.Repositories
{
    interface IProductRepository : IRepository<Product>
    {
    }
}
