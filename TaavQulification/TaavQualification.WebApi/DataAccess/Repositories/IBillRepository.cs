﻿using TaavQualification.WebApi.DataAccess.Entities;

namespace TaavQualification.WebApi.DataAccess.Repositories
{
    interface IBillRepository : IRepository<Bill>
    {
    }
}
