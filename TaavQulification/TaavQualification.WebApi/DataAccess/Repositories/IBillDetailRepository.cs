﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TaavQualification.WebApi.DataAccess.Entities;

namespace TaavQualification.WebApi.DataAccess.Repositories
{
    interface IBillDetailRepository : IRepository<BillDetail>
    {
    }
}
