using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using TaavQualification.WebApi.DataAccess.Context;
using TaavQualification.WebApi.DataAccess.UnitOfWorks;

namespace TaavQualification.WebApi
{
    public class Startup
    {
        private readonly IConfiguration _configuration;
        public static int PageSize = 0;

        public Startup(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            var con = _configuration["ConnectionStrings:DefaultConnection"];
            if (!int.TryParse(_configuration["PageSize"], out PageSize))
            {
                PageSize = 10;
            }

            services
            //.AddMvc(/*options => options.UseGlobalRoutePrefix(appSettings.ServiceName)*/)
            .AddMvc(options =>
            {
                options.AllowEmptyInputInBodyModelBinding = true; // false by default
            })
            .SetCompatibilityVersion(CompatibilityVersion.Version_3_0);
            services.AddDbContext<AppDbContext>(builder => builder.UseSqlServer(con));
            services.AddApiVersioning(options => options.ReportApiVersions = true);

            services.AddTransient<IUnitOfWork, UnitOfWork>();
            services
            .AddControllers()
            .AddNewtonsoftJson(opt =>
            {
                opt.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
